"use strict";


// Упражнение 1

 for (let Num = 0; Num <= 20; Num++){
   if(Num%2==0 &&  Num!=0) console.log(Num);           // выводим все четные числа, включая число 20(за исключением нуля).
 }

 // Вариант №2 с циклом while
 /*let Num = 0;
 while (Num <= 20) {
   if (Num%2==0 &&  Num!=0) {
   console.log(Num); 
 }
   Num ++;
 }*/
 
 
// Упражнение 2

 let sum = 0;
 let num = 3;
 while(num) {                                 // цикл повторяется три раза(пока num!=0).
   let value = +prompt("Введите число");
   if(!value && value!=0){                    // если введеное значение не число ("0"- является числом), выводим ошибку и завершаем цикл;
      alert("Ошибка, Вы ввели не число");
      break;
   }
   sum+= value;
   num--;
}
alert("Сумма: "+ sum);


// Упражнение 3

function getNameOfMonth(month) {
   return (month === 0) ? 'Январь': 
   (month == 1) ? 'Февраль': 
   (month == 2) ? 'Март': 
   (month == 3) ? 'Апрель': 
   (month == 4) ? 'Май': 
   (month == 5) ? 'Июнь':
   (month == 6) ? 'Июль':
   (month == 7) ? 'Август':
   (month == 8) ? 'Сентябрь':
   (month == 9) ? 'Октябрь':
   (month == 10) ? 'Ноябрь':
   (month == 11) ? 'Декабрь':
   '';
}
 
for (let month = 0; month < 12; month++){
   if (month===9) continue;
   
   console.log(getNameOfMonth(month));
}


// Упражнение 4

// Immediately-Invoked Function Expressions» (аббревиатура IIFE), что означает функцию, запускаемую сразу после объявления.
// Чтобы код выполнился сразу, пишем безымянную функцию в скобках, а затем добавляете скобки после нее, чтобы вызвать ее.
// Пример:
(function() {     

   alert('Hello, Frontenders!');
 
 })(); 