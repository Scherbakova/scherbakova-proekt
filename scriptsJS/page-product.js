"use strict";

// Cохранение данных в LocalStorage

let searchForm = document.querySelector(".search-form");

let nameForm = searchForm.querySelector(".search-form__name");
let ratingForm = searchForm.querySelector(".search-form__rating");

let inputName = nameForm.querySelector(".input-box__name");
let inputRating = ratingForm.querySelector(".input-box__rating");
let outputErrorName = nameForm.querySelector(".input-box__error-name");
let outputErrorRating = ratingForm.querySelector(".input-box__error-rating");

let inputReview = searchForm.querySelector(".textarea");

document.addEventListener("DOMContentLoaded", function () {
  for (let input of searchForm.querySelectorAll(".input")) {
    // Задаем всем элементам, с классом "input", событие при вводе нового значения в поле ввода.

    input.addEventListener("input", (event) => {
      let value = event.target.value;
      let name = event.target.getAttribute("id"); //Задаем ключ по аттрибут "id"- элемента.
      localStorage.setItem(name, value);
    });
  };

  // Присваиваем полям ввода значение сохраненное в LocalStorage.
  inputName.value = localStorage.getItem("name");
  inputRating.value = localStorage.getItem("rating");
  inputReview.value = localStorage.getItem("review");

  function handleSubmit(event) {
    event.preventDefault();

    let name = inputName.value.replaceAll(" ", ""); // Убираем все пробелы.

    let nameError = (name.length === 0) ? "Вы забыли указать имя и фамилию":
    (name.length < 2) ? "Имя не может быть короче 2-х символов":
    "";

    outputErrorName.innerText = nameError;

    if (nameError) {
      outputErrorName.classList.add("error-blok");
      inputName.classList.add("border-red");
      return;
    } else {
      outputErrorName.classList.remove("error-blok");
      inputName.classList.remove("border-red");
    }

    let rating = inputRating.value;
    let ratingError = "";
    if (isNaN(rating) || rating < 1 || rating > 5 || rating.length == 0) {
      ratingError = "Оценка должна быть от 1 до 5";
    }

    outputErrorRating.innerText = ratingError;

    if (ratingError) {
      outputErrorRating.classList.add("error-blok");
      inputRating.classList.add("border-red");
    } else {
      outputErrorRating.classList.remove("error-blok");
      inputRating.classList.remove("border-red");
    }

    // Очищаем поля ввода, после отправки формы.
    if (!ratingError && !nameError && !inputReview.value == "") {
      localStorage.removeItem("name");
      inputName.value = "";
      localStorage.removeItem("rating");
      inputRating.value = "";
      localStorage.removeItem("review");
      inputReview.value = "";
    }
  }
    // Убираем ошибку при вводе нового значения вполя ввода.
  function handleFocus(event) {
    inputName.classList.remove("border-red");
    outputErrorName.classList.remove("error-blok");

    inputRating.classList.remove("border-red");
    outputErrorRating.classList.remove("error-blok");
  }

  searchForm.addEventListener("submit", handleSubmit);
  inputName.addEventListener("focus", handleFocus);
  inputRating.addEventListener("focus", handleFocus);

  // Промежуточная аттестация.

  //Корзина покупок.
  let cart = {};
  //Избранное.
  let like = {};
  //Счетчик корзины.
  let countBasket = document.querySelector(".basket-counter").innerText;
  // Счетчик избранного.
  let countHeart = document.querySelector(".heart-counter").innerText;
  
  let buttonInCart = document.querySelector(".important .btn");
  let heart = document.querySelector(".important__heart-icon");
  //Удаляет из корзины товар.
  function cleanFromCart() {
    buttonInCart.innerText = "Добавить в корзину";
    buttonInCart.classList.remove("btn-active");
    buttonInCart.classList.add("important__in-btn", "btn");
    document.querySelector(".in-basket").classList.remove("selected");
    document.querySelector(".basket-counter").innerText = "";
    countBasket = document.querySelector(".basket-counter").innerText;
  };
  //Добавляет в корзину товар.
  function addInCart() {
    buttonInCart.innerText = "Товар уже в корзине";
    document.querySelector(".in-basket").classList.add("selected");
    buttonInCart.classList.remove("important__in-btn", "btn");
    buttonInCart.classList.add("btn-active");
    document.querySelector(".basket-counter").innerText = "1";
    countBasket = document.querySelector(".basket-counter").innerText;
  }
  //Защита от перезагрузок, сохраняем в localStorage
  if (localStorage.getItem("countBasket")) {
    countBasket = localStorage.getItem("countBasket");
    addInCart();
  };
  
  if (localStorage.getItem("countHeart")) {
    countHeart = localStorage.getItem("countHeart");
    addInLike();
  };
  
  if (localStorage.getItem("cart")) {
    cart = JSON.parse(localStorage.getItem("cart"));
  };
  
  if (localStorage.getItem("like")) {
    like = JSON.parse(localStorage.getItem("like"));
  };
  //По клику добавляем и удаляем товар из корзины.
  function handleClickBtn(event) {
    let price = event.target.dataset.price;
    let title = event.target.dataset.title;
    let id = event.target.dataset.id;
    let product = {
      title: title,
      price: price,
      count: 1,
    };
  
    if (id in cart) {                           
      for (let key of Object.keys(cart)) delete cart[key];
      cleanFromCart();
    } else {
      cart[id] = product;
      addInCart();
    };
    //Сохроняем в localStorage данные из корзины.
    localStorage.setItem("countBasket", countBasket);
    localStorage.setItem("cart", JSON.stringify(cart))
  };
  
  //Удаляет из избранного товар.
  function cleanFromLike() {
    document.querySelector(".heart-fill").classList.remove("svg-action");
    document.querySelector(".in-heart").classList.remove("selected");
    document.querySelector(".heart").classList.remove("svg-red");
    document.querySelector(".heart-counter").innerText = "";
    countHeart = document.querySelector(".heart-counter").innerText;
  };
  //Добавляем в избранное товар.
  function addInLike() {
    document.querySelector(".in-heart").classList.add("selected");
    document.querySelector(".heart-fill").classList.add("svg-action");
    document.querySelector(".heart").classList.add("svg-red");
    document.querySelector(".heart-counter").innerText = "1";
    countHeart = document.querySelector(".heart-counter").innerText;
  }
  //По клику добавляем и удаляем товар из избранного.
  function handleClickHeart(event) {
    let title = event.target.dataset.title;
    let id = event.target.dataset.id;
    let product = {
      title: title,
    };
    
    if (id in like) {                           
      for (let key of Object.keys(like)) delete like[key];
      cleanFromLike();
    } else { 
      like[id] = product;
      addInLike();
    };
    //Сохроняем в localStorage данные из избранного.
    localStorage.setItem("countHeart", countHeart);
    localStorage.setItem("like", JSON.stringify(like));
  };
  
  buttonInCart.addEventListener("click", handleClickBtn);
  heart.addEventListener("click", handleClickHeart);
 
});
