"use strict";


// Упражнение 1

/**
 * Вычисляет сумму чисел в массиве
 * @param {object} arr - массив
 * @returns {number} - сумма чисел в массиве
 */
function getSumm(arr) {
  let sum = 0;
  for (let number of arr) {
    if (!Number.isFinite(number)) continue; // проверка, является ли значение конечным числом.
    sum += number;
  }
  return sum;
}

let arr1 = [1, 2, 10, 5];
console.log(getSumm(arr1)); 
let arr2  = ["a", {}, 3, 3, -2];
console.log(getSumm(arr2));

// Упражнение 2
// Добавлено в отдельном файле data.js.


// Упражнение 3
 
// Вариант №1
// В корзине один товар
let cart = [4884];  

/**
 * Добавляем в корзину товар.
 * @param {number} IdProduct - идентификатор товара.
 * @returns - корзина(массив) с добавленным товара.
 */
function addToCart(IdProduct) {
  let inCart = cart.includes(IdProduct); // проверка наличия товара в корзине.
  if (inCart) return;
  cart.push(IdProduct); // добавляем товар.
}

// Добавили товар
addToCart(3456); 
console.log(cart);
// Повторно добавили товар
addToCart(3456);
console.log(cart);

/**
 * Удаляем из корзины товар.
 * @param {number} IdProduct - идентификатор товара.
 * @returns - корзина(массив) с удаленным  товара.
 */
function  removeFromCart(IdProduct) {
  cart = cart.filter((item) => item !== IdProduct); // фильтруем массив, оставляем все, кроме указанного товара.
  return cart;
}
// Удалили товар
removeFromCart(4884);
console.log(cart)

/*
// Вариант №2 через Set.

let cart = new Set([4884]);

function addToCart(idProduct) {
  return cart.add(idProduct);
}

addToCart(3456);
addToCart(3456); // Повторно добавили товар
console.log(Array.from(cart)); // Преобразуем в массив

function  removeFromCart(idProduct) {
  return cart.delete(idProduct);
}

removeFromCart(4884);
console.log(Array.from(cart));
*/


