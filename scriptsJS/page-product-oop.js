"use strict";

class Form {
  constructor(searchForm, inputName, inputRating, inputReview, outputErrorName, outputErrorRating) {
    this.searchForm = searchForm;
    this.inputName = inputName;
    this.inputRating = inputRating;
    this.inputReview = inputReview;
    this.outputErrorName = outputErrorName;
    this.outputErrorRating = outputErrorRating;
  }

  handleInput(e) {
    let value = e.target.value;
    let name = e.target.getAttribute("id");
    localStorage.setItem(name, value);
  }

  saveLocalStorage() {
    // Записываем полям ввода значение сохраненное в LocalStorage.
    inputName.value = localStorage.getItem("name");
    inputRating.value = localStorage.getItem("rating");
    inputReview.value = localStorage.getItem("review");
  }

}

class AddReviewForm extends Form{
  
  validateForm(e) {
    e.preventDefault()
    // Валидация поля ввода имени
    let name = inputName.value.replaceAll(" ", "");

    let nameError = (name.length === 0) ? "Вы забыли указать имя и фамилию":
      (name.length < 2) ? "Имя не может быть короче 2-х символов"
       : "";
    outputErrorName.innerText = nameError;

    if (nameError) {
      outputErrorName.classList.add("error-blok");
      inputName.classList.add("border-red");
      return;
    } else {
      outputErrorName.classList.remove("error-blok");
      inputName.classList.remove("border-red");
    }

    // Валидация поля ввода рейтинга
    let rating = inputRating.value;
    let ratingError = "";
    if (isNaN(rating) || rating < 1 || rating > 5 || rating.length == 0) {
      ratingError = "Оценка должна быть от 1 до 5";
    }
    outputErrorRating.innerText = ratingError;

    if (ratingError) {
      outputErrorRating.classList.add("error-blok");
      inputRating.classList.add("border-red");
    } else {
      outputErrorRating.classList.remove("error-blok");
      inputRating.classList.remove("border-red");
    }

    //Очищаем поля ввода, после отправки формы.
    if (!ratingError && !nameError && !inputReview.value == "") {
      localStorage.removeItem("name");
      inputName.value = "";
      localStorage.removeItem("rating");
      inputRating.value = "";
      localStorage.removeItem("review");
      inputReview.value = "";
    }
  }

  //Убираем сообщения ошибки при фокусе на поля ввода имени.
  handleFocusName() {
    inputName.classList.remove("border-red");
    outputErrorName.classList.remove("error-blok");
  }
  
  //Убираем сообщения ошибки при фокусе на поля ввода рейтинга.
  handleFocusRating() {
    inputRating.classList.remove("border-red");
    outputErrorRating.classList.remove("error-blok");
  }
}

let searchForm = document.querySelector(".search-form");
let nameForm = searchForm.querySelector(".search-form__name");
let ratingForm = searchForm.querySelector(".search-form__rating");

let inputName = nameForm.querySelector(".input-box__name");
let inputRating = ratingForm.querySelector(".input-box__rating");
let inputReview = searchForm.querySelector(".textarea");
let outputErrorName = nameForm.querySelector(".input-box__error-name");
let outputErrorRating = ratingForm.querySelector(".input-box__error-rating");

let formReview = new AddReviewForm(searchForm, inputName, inputRating, inputReview, outputErrorName, outputErrorRating);

for (let input of searchForm.querySelectorAll(".input")) {
  input.addEventListener("input", formReview.handleInput);
  formReview.saveLocalStorage();
};

searchForm.addEventListener("submit", formReview.validateForm);

inputName.addEventListener("focus", formReview.handleFocusName);
inputRating.addEventListener("focus", formReview.handleFocusRating);
