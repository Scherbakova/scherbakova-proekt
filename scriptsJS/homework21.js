"use strict";


// Упражнение 1
/**
 * Определяем наличия в объекте свойств.  
 * @param {object} obj - объект
 * @param  key - свойство объекта.
 * @returns {boolean}  - возвращает true, если у объекта нет свойств, иначе false.
 */

function isEmpty(obj) {  // 
  for(let key in obj) {
    return false;
  }
  return true;
}

// Для проверки кода
/*let user= {}; 
user.age = 10;
console.log(isEmpty(user));*/

// Упражнение 2
// Сделано в отдельном файле data.js.


// Упражнение 3

let salaries = {
  John: 100000,
  Ann: 160000,
  Pete: 130000,
};

/**
 * Функция производит повышение зарплаты на определенный процент.
 * @param {number} perzent - процент повышения зарплаты.
 * @returns {object}  - объект с новыми зарплатами.
 */
function raiseSalary(perzent) {
  let newObject = {}; 

  for (let key in salaries) {
    let raise = salaries[key] * perzent/100;          // величина повышения зарплат.
    newObject[key] = salaries[key] + Math.floor(raise); // округление до целого числа в меньшую сторону.
  }

  return newObject; 
}

console.log(raiseSalary(5));

let newSalaries = raiseSalary(5);
let count = 0;

  for (let key in newSalaries) {
    if (typeof newSalaries[key] == 'number') {    // проверка, что значение свойства числовое.
      count += newSalaries[key];
    } 
  }

console.log(count); // суммарное значение всех зарплат.


// Вариант №2
// С помощью функции рассчитываем общий бюджет.
/**
 * Функция рассчитывает суммарное значение всех зарплат.
 * @param {object} objSalaries - объект с зарплатами.
 * @returns {number} - общий бюджет.
 */
 /*function countSalary(objSalaries) {
  let count = 0;

  for (let key in objSalaries) {
    count += objSalaries[key];
  }
  
  return count;
}

console.log(countSalary(newSalaries));*/


