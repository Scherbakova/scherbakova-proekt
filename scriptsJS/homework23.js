"use strict";

// Упражнение 1

let number = prompt("Введите число");

if(!isNaN(number)){
  let countdownTimer = setInterval(()=> {
    if(number > 0) {                      // число должно быть положительным, не равным нолю.
      console.log(`Осталось ${number}`)  // запускаем таймер, начиная с запрошенного числа.
      number--;
    } else {
      clearInterval(countdownTimer);  // как только число станет равно нулю, остановливаем таймер.
      console.log("Время вышло!")
    }
  }, 1000);
} else {
  alert("Ошибка! Вы ввели не число!");
};

// Вариант №2 с помощью Promise:
/*let promise = new Promise(function (resolve, reject) {
  let number = prompt("Введите число");
  if(!isNaN(number)) {
    let value = number;
    resolve(value);  
  } else {
    let error = new Error (`Ошибка! Вы ввели не число!`);
    reject(error);
  }
});

promise
  .then(function (value) {
    let countdownTimer = setInterval(()=> {
      if(value > 0) {                      
        console.log(`Осталось ${value}`)  
        value--;
      } else {
        clearInterval(countdownTimer);  
        console.log("Время вышло!")
      }
    }, 1000);
    
  })
  .catch(function (error) {
    alert(error.message);
  });*/

  
// Упражнение 2

fetch("https://reqres.in/api/users")
  .then( response => {
    return response.json();
  })
  .then( response => {
    let userData = response.data;
    console.log(`Получили пользователей: ${userData.length}`); // количество пользователей
    userData.forEach((user) => console.log(`— ${user.first_name} ${user.last_name} (${user.email})`)); // имя, фамилию и email каждого пользователя
  });

  // Дополнительное, надеюсь допишу позже (есть наброски):) 
  
 


