import React from "react";
import LinkPage from "../LinkPage";
import styles from "./Characteristics.module.css";


function Characteristics(props) {
  
  return (
    <section className={styles.options}>
      <h3 className={styles.subtitle}>Характеристики товара</h3>
      <ul className={styles.list}>
        <li className={styles.item}>
          Экран: <b>6.1</b>
        </li>
        <li className={styles.item}>
          Встроенная память: <b>128 ГБ</b>
        </li>
        <li className={styles.item}>
          Операционная система: <b>iOS 15</b>
        </li>
        <li className={styles.item}>
          Беспроводные интерфейсы: <b>NFC, Bluetooth, Wi-Fi</b>
        </li>
        <li className={styles.item}>
          Процессор:
          <LinkPage
            className={styles.link}
            href={"https://ru.wikipedia.org/wiki/Apple_A15"}
            target={"_blank"}
            rel={"noopener noreferrer"}
            text={<b>Apple A15 Bionic</b>}
          />
        </li>
        <li className={styles.item}>
          Вес: <b>173 г</b>
        </li>
      </ul>
    </section>
  );
}
export default Characteristics;
