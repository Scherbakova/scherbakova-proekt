import React from "react";
import "./ImagesList.css";

function ImagesList(images) {
  images = [
    {src: '/Images/Image1.webp', alt: "Изображение передней и задней панели смартфона Apple"},
    {src: '/Images/Image2.webp', alt: "Изображение передней панели смартфона Apple"},
    {src: '/Images/Image3.webp', alt: "Изображение передней и задней панели смартфона Apple под углом"},
    {src: '/Images/Image4.webp', alt: "Изображение камеры на задней панели смартфона Apple"},
    {src: '/Images/Image5.webp', alt: "Изображение задней и передней панели смартфона Apple"},
  ];
  return (
    <div className="list-images">
      {images.map((image) => (
        <img
          key={image.src}
          className="list-images__item"
          src={image.src}
          title="Смартфон Apple iPhone 13"
          alt={image.alt}
        />
      ))}
    </div>
  );
}

export default ImagesList;
