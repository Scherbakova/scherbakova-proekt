import React from "react";
import "./SliderImges.css";

function SliderImges(images) {
  images = [
    {src: '/Images/Image1.webp', alt: "Изображение передней и задней панели смартфона Apple"},
    {src: '/Images/Image2.webp', alt: "Изображение передней панели смартфона Apple"},
    {src: '/Images/Image3.webp', alt: "Изображение передней и задней панели смартфона Apple под углом"},
    {src: '/Images/Image4.webp', alt: "Изображение камеры на задней панели смартфона Apple"},
    {src: '/Images/Image5.webp', alt: "Изображение задней и передней панели смартфона Apple"},
  ];
  return (
    <div className="slider-container">
      <input
        defaultChecked
        type="radio"
        name="react"
        id="desktop"
        className="slider-container__desktop input-slider"
      />
      <div className="slider">
        <input
          defaultChecked
          type="radio"
          name="slider"
          id="switch1"
          className="slider__switch1 input-slider"
        />
        <input
          type="radio"
          name="slider"
          id="switch2"
          className="slider__switch2 input-slider"
        />
        <input
          type="radio"
          name="slider"
          id="switch3"
          className="slider__switch3 input-slider"
        />
        <input
          type="radio"
          name="slider"
          id="switch4"
          className="slider__switch4 input-slider"
        />
        <input
          type="radio"
          name="slider"
          id="switch5"
          className="slider__switch5 input-slider"
        />
        <div className="slides">
          <div className="slides__overflow">
            <div className="slides__list-images">
              {images.map((image) => (
                <div className="slides__list-item" key={image.src}>
                  <img
                    className="slides__img"
                    src={image.src}
                    title="Смартфон Apple iPhone 13"
                    alt={image.alt}
                  />
                </div>
              ))}
            </div>
          </div>
        </div>
        <div className="slider__controls">
          <label className="label" htmlFor="switch1" />
          <label className="label" htmlFor="switch2" />
          <label className="label" htmlFor="switch3" />
          <label className="label" htmlFor="switch4" />
          <label className="label" htmlFor="switch5" />
        </div>
        <div className="slider__active">
          <label className="label" htmlFor="switch1" />
          <label className="label" htmlFor="switch2" />
          <label className="label" htmlFor="switch3" />
          <label className="label" htmlFor="switch4" />
          <label className="label" htmlFor="switch5" />
        </div>
      </div>
    </div>
  );
}

export default SliderImges;
