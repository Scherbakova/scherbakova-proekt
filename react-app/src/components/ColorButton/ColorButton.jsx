import React from "react";
import "./ColorButton.css";
import {memo} from "react";

function ColorButton(props) {
    let {color, className, onClick} = props;
    
    return (
        <button className={`product-color__item ${className}`} onClick={() => onClick(color.title)}> 
            <img src={color.src} title={color.title} alt={color.alt} className="product-color__img-color" />
        </button>
    );
}
// В паре с хуком useCallback не позволяет рендеринге при изменении props.
export default memo(ColorButton);
