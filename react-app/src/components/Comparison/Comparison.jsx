import React from "react";
import "./Comparison.css";

function Comparison(products) {
  products = [
    {type: "Iphone11", weight: "194 грамма", height: "150.9 мм", width: "75.7 мм", depth: "8.3 мм", сhip: "A13 Bionicchip", memory: "до 128 Гб", battery: "До 17 часов"},
    { type: "Iphone12", weight: "164 грамма", height: "146.7 мм", width: "71.5 мм", depth: "7.4 мм", сhip: "A14 Bionicchip", memory: "до 256 Гб", battery: "До 19 часов"},
    {type: "Iphone13", weight: "174 грамма", height: "146.7 мм", width: "71.5 мм", depth: "7.65 мм", сhip: "A15 Bionicchip", memory: "до 512 Гб", battery: "До 19 часов"},
  ];
  return (
    <section className="model-comparison ">
      <h3 className="model-comparison__subtitle">Сравнение моделей</h3>
      <div className="model-comparison__box-table">
        <table className="table">
          <thead className="table__thead">
            <tr className="table__thead-tr">
              <th className="table__th">Модель</th>
              <th className="table__th">Вес</th>
              <th className="table__th">Высота</th>
              <th className="table__th">Ширина</th>
              <th className="table__th">Толщина</th>
              <th className="table__th">Чип</th>
              <th className="table__th">Объём памяти</th>
              <th className="table__th">Аккумулятор</th>
            </tr>
          </thead>
          <tbody className="table__tbody">
            {products.map((product) => (
              <tr className="table__tbody-tr" key={product.type}>
                <td className="table__td">{product.type}</td>
                <td className="table__td">{product.weight}</td>
                <td className="table__td">{product.height}</td>
                <td className="table__td">{product.width}</td>
                <td className="table__td">{product.depth}</td>
                <td className="table__td">{product.сhip}</td>
                <td className="table__td">{product.memory}</td>
                <td className="table__td">{product.battery}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </section>
  );
}

export default Comparison;
