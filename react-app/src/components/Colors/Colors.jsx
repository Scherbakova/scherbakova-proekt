import React from "react";
import "./Colors.css";
import {useState, useMemo, useCallback} from "react";
import ColorButton from "../ColorButton";

function Colors(colors) {
  let [selectedColor, setSelectedColor] = useState("Синий");
  // Использован хук useMemo, он позволяет запоминать функцию без необходимости их вызова при каждом новом рендеринге.
  // Использован т.к. здесь измененяется состояния, а также происходит рендеринге дочерних элементов с помощью Array.map()
  colors = useMemo(() => ( [
    {src: "/colorRed.webp", title: "Красный", alt: "Красный смартфона AppleiPhone 13"},
    {src: "/colorGreen.webp", title: "Зеленый", alt: "Зеленый смартфона AppleiPhone 13"},
    {src: "/colorPink.webp", title: "Розовый", alt: "Розовый смартфона AppleiPhone 13"},
    {src: "/colorBlue.webp", title: "Синий", alt: "Синий смартфона AppleiPhone 13"},
    {src: "/colorWhite.webp", title: "Белый", alt: "Белый смартфона AppleiPhone 13"},
    {src: "/colorBlack.webp", title: "Черный", alt: "Черный смартфона AppleiPhone 13"},
  ]), []);
  //Использован хук useCallback, который так же как и useMemo оптимизирует рендеринг компонентов, использован в паре с компонетом React.Memo. 
  let onClick = useCallback((title) => setSelectedColor(title), []);
  
  return (
    <section className="product-color">
        <h3 className="product-color__subtitle">Цвет товара: {`${selectedColor}`}</h3>
        <div className="product-color__items">
          {colors.map((color) => (
            <ColorButton
              key={color.title}
              color={color}
              className ={`${(color.title === selectedColor) ? "product-color__item_active": ""}`}
              onClick={onClick}
              title={color.title}
            />
          ))}
        </div>
    </section>
  );
}
export default Colors;
