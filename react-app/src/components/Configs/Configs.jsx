import React from "react";
import "./Configs.css";
import {useState, useMemo, useCallback} from "react";

import ConfigButton from "../ConfigButton";


function Configs(numbers) {
  let [selectedButton, setSelectedButton] = useState(128);
  // Использован хук useMemo, он позволяет запоминать функцию без необходимости их вызова при каждом новом рендеринге.
  // Использован т.к. здесь измененяется состояния, а также происходит рендеринге дочерних элементов с помощью Array.map()
  numbers = useMemo(() => ([128, 256, 512]), []);

  //Использован хук useCallback, который так же как и useMemo оптимизирует рендеринг компонентов, использован в паре с компонетом React.Memo. 
  let onclick = useCallback((number) => setSelectedButton(number), []);

  return (
    <section className="memory">
      <h3 className="memory__subtitle">Конфигурация памяти: {`${selectedButton} ГБ`}</h3>
      <div className="memory__btns">
        {numbers.map((number) => (
          <ConfigButton
            key = {number}
            text={`${number} ГБ`}
            className = {`${(number===selectedButton) ? "btn_active": ""}`}
            onClick = {onclick}
            number ={number}
          />
        ))}
      
      </div>
    </section>
  );
}

export default Configs;
