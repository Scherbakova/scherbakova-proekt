import React from "react";
import "./Reviews.css";
import Review from '../Review';


function Reviews(props) {
  let {reviews} = props;
  return (
    <section className="reviews reviews_size_m">
      <div className="reviews__header">
        <div className="reviews__box-text">
          <h3 className="reviews__subtitle">Отзывы</h3>
          <div className="reviews__amount">
            <span>425</span>
          </div>
        </div>
      </div>
      <div className="reviews__list">
        {reviews.map((review) => (
          <Review
            key={review.id}
            review={review}
            selected={review.id < reviews.length}
          />
        ))}
      </div>
    </section>
  );
}

export default Reviews;
