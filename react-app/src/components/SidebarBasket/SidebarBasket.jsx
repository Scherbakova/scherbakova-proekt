import React from "react";
import "./SidebarBasket.css";

import { BsSuitHeartFill, BsSuitHeart } from "react-icons/bs";

import { useSelector, useDispatch } from "react-redux";
import { addProduct, removeProduct } from "../../Reducers/basket-reducer";
import { addSelected, removeSelected } from "../../Reducers/heart-reducer";


function SidebarBasket(props) {
  let {product} = props;
  
  // Получаем частичку глобального состояния
  let basketProducts = useSelector((state) => state.basket.products);
  let heartProducts = useSelector((state) => state.heart.products);
  let dispatch = useDispatch();

  let productInBasket = basketProducts.some((prevProduct) => {
    return prevProduct.id === product.id;
  });

  let productInHeart = heartProducts.some((prevProduct) => {
    return prevProduct.id === product.id;
  });

  let btnBasketText = `${productInBasket ? "Товар уже в корзине" : "Добавить в корзину"}`;
  let classNameBtn = `${productInBasket ? "btn-active" : "important__in-btn btn"}`;

  
  let handleClickBtn = () => {
    if (productInBasket) {
      dispatch(removeProduct(product));
    } else {
      dispatch(addProduct(product));
    }
  };
  
  let handleClickLike = () => {
    if (productInHeart) {
      dispatch(removeSelected(product));
    } else {
      dispatch(addSelected(product));
    }
  };
  
  return (
    <div className="important">
      <div className="important__header">
        <div className="important__price">
          <div className="important__price-old">
            <p className="important__price-old-text">{product.priceOldRub}</p>
            <div className="important__discount">
              <p className="important__discount-text">{product.discount}</p>
            </div>
          </div>
          <p className="important__price-new">
            <b>{product.priceNewRub}</b>
          </p>
        </div>
        {/* Добавить в избранное */}
        <div className="important__heart-icon" onClick={() => handleClickLike(product)}>
          {productInHeart ? (
            <BsSuitHeartFill className="important__svg-add" />
          ) : (
            <BsSuitHeart className="important__svg" />
          )}
        </div>
      </div>
      <div className="important__delivery">
        <p className="important__delivery-text">
          Самовывоз в четверг, 1 сентября — <b>бесплатно</b>
        </p>
        <p className="important__delivery-text">
          Курьером в четверг, 1 сентября — <b>бесплатно</b>
        </p>
      </div>
      {/* Добавить в корзину */}
      <button
        onClick={() => handleClickBtn(product)}
        className={classNameBtn}
      >
        {`${btnBasketText}`}
      </button>
    </div>
  );
}
export default SidebarBasket;
