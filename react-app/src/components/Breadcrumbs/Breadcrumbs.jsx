import React from "react";
import "./Breadcrumbs.css";
import LinkPage from "../LinkPage";

function Breadcrumbs(items) {
  items = [
    { id: "1", href: "#", text: "Электроника" },
    { id: "2", href: "#", text: "Смартфоны и гаджеты" },
    { id: "3", href: "#", text: "Мобильные телефоны" },
    { id: "4", href: "#", text: "Apple" },
  ];

  return (
    <ul className="breadcrumbs">
      {items.map((item) => (
        <li key={item.id}>
            <LinkPage
                className={"breadcrumbs__link"}
                href={item.href}
                text={item.text}
            />
        </li>
      ))}
    </ul>
  );
}

export default Breadcrumbs;
