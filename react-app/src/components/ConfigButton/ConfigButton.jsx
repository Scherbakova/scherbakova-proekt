import React from "react";
import "./ConfigButton.css";
import {memo} from "react";

function ConfigButton(props) {
  let {text, className, onClick, number} = props;
  return (
    <button className = {`btn ${className}`} onClick = {() => onClick(number)}>
      {text}
    </button>
  )
};
// В паре с хуком useCallback не позволяет рендеринге при изменении props.
export default memo(ConfigButton);