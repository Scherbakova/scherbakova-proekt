import React from "react";
import "./Logo.css";
import { Link } from "react-router-dom";

function Logo(props) {
  let {src = "/LogoIcon.png"} = props;
  return (
    <Link to="/" className={"logo__title"}>
      <div className="logo header__logo">
        <img className="logo__img" src={src} alt="Логотип" />
        <h1 className="logo__title">
          <span className="logo__text-color">Мой</span>Маркет
        </h1>
      </div>
    </Link>
  );
}

export default Logo;
