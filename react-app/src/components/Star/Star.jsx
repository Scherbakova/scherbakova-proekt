import React from "react";
import "./Star.css";

import {ImStarFull} from "react-icons/im"

function Star(props) {
  let {color = "grey"} = props;

  return (
    <ImStarFull
      className="feedback__rating-star"
      color={color}
    />
  );
}

export default Star;
