import React from "react";
import "./DateYear.css";
import {useCurrentDate} from "@kundinos/react-hooks";

function DateYear() {
  const currentDate = useCurrentDate();
  const fullYear = currentDate.getFullYear();
  
  return (
    <div className="fullYearBox">
      <p className="fullYear">{`${fullYear}`}</p>
    </div>
  );
}

export default DateYear;