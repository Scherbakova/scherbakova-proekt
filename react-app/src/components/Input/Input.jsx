import React from "react";
import "./Input.css";

function Input(props) {
  let {value, type, id, name, placeholder, focus, input, className} = props;

  return (
    <input
      onFocus={focus}
      onInput={input}
      value={value}
      className={className}
      type={type}
      id={id}
      name={name}
      placeholder={placeholder}
    />
  );
}

export default Input;
