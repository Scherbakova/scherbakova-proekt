import React from "react";
import styled from "styled-components";

let Container = styled.section`
  display: flex;
  flex-direction: column;
  gap: 15px;
`;

let Subtitle = styled.h3`
  font-weight: 700;
  font-size: 16px;
  line-height: 19px;
  margin: 0;

  @media screen and (max-width: 799px) {
    font-size: 14px;
    line-height: 17px;
  }
`;

let Box = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  padding: 0;
  gap: 15px;
`;

let Text = styled.p`
  white-space: pre-wrap;
  font-weight: 500;
  font-size: 16px;
  line-height: 19px;
  margin: 0;

  @media screen and (max-width: 799px) {
    font-size: 14px;
    line-height: 17px;
  }
`;

let TextItalic = styled.span`
  font-weight: 400;
  font-size: 16px;
  line-height: 19px;
  font-style: italic;

  @media screen and (max-width: 799px) {
    font-size: 14px;
    line-height: 17px;
  }
`;

function Description() {
  return (
    <Container>
      <Subtitle>Описание</Subtitle>
      <Box>
        <Text>
          Наша самая совершенная система двух камер.{"\n"}
          Особый взгляд на прочность дисплея. {"\n"}
          Чип, с которым всё супербыстро. {"\n"}
          Аккумулятор держится заметно дольше. {"\n"}
          <TextItalic>
            iPhone 13 - сильный мира всего.
          </TextItalic>
        </Text>
        <Text>
          Мы разработали совершенно новую схему расположения и развернули
          объективы на 45 градусов. Благодаря этому внутри корпуса поместилась
          наша лучшая система двух камер с увеличенной матрицей широкоугольной
          камеры. Кроме того, мы освободили место для системы оптической
          стабилизации изображения сдвигом матрицы. И повысили скорость работы
          матрицы на сверхширокоугольной камере.
        </Text>
        <Text>
          Новая сверхширокоугольная камера видит больше деталей в тёмных
          областях снимков. Новая широкоугольная камера улавливает на 47% больше
          света для более качественных фотографий и видео. Новая оптическая
          стабилизация со сдвигом матрицы обеспечит чёткие кадры даже в
          неустойчивом положении.
        </Text>
        <Text>
          Режим «Киноэффект» автоматически добавляет великолепные эффекты
          перемещения фокуса и изменения резкости. Просто начните запись видео.
          Режим «Киноэффект» будет удерживать фокус на объекте съёмки, создавая
          красивый эффект размытия вокруг него. Режим «Киноэффект» распознаёт,
          когда нужно перевести фокус на другого человека или объект, который
          появился в кадре. Теперь ваши видео будут смотреться как настоящее
          кино.
        </Text>
      </Box>
    </Container>
  );
}

export default Description;
