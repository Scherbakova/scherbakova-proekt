import React from 'react';
import './Header.css';
import Logo from '../Logo';
import CounterIcon from '../CounterIcon';

import HeartHeader from './HeartHeader.svg';
import BasketHeader from './BasketHeader.svg';

import {useSelector} from "react-redux";


function Header() {
  let countBasket = useSelector ((state) => state.basket.products.length);
  let countHeart = useSelector ((state) => state.heart.products.length);
  
  return (
    <header className="header page__header">
      <div className="header__content">
        <Logo/>
        <div className="header__icons">
          <CounterIcon
          className1={"header__img-heart"}
          className2={`in-heart ${(countHeart > 0) ? "selected": ""}`}
          className3={"heart-counter"}
          count = {countHeart > 0 && countHeart}
          src={HeartHeader}
          alt={"Сердце"}
          id={"heart-counter"} />
          <CounterIcon
          className1={"header__img-basket"}
          className2={`in-basket ${(countBasket > 0) ? "selected": ""}`}
          className3={"basket-counter"}
          count = {countBasket > 0 && countBasket}
          src={BasketHeader}
          alt={"Корзина"}
          id={"basket-counter"} />
        </div>
      </div>
    </header>
  );
}

export default Header;