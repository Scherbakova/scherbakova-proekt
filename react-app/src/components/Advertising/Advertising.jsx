import React from "react";
import "./Advertising.css";


function Advertising() {
  return (
    <div className="ad">
      <p className="ad__subtitle">Реклама</p>
      <div className="ad-list">
        <iframe src="./ad.html" title='Реклама' className="ad-blok ad-list__ad-blok"/>
        <iframe src="./ad.html" title='Реклама' className="ad-blok ad-list__ad-blok"/>
      </div>
    </div>
  );
}

export default Advertising;
