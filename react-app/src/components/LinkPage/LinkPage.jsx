import React from 'react';
import './LinkPage.css';

function LinkPage(props) {
    let {className, href, target, text}=props;
    return ( 
        <a className={`a-link ${className}`} href={href} target={target}> {text} </a>
    );
}

export default LinkPage;
