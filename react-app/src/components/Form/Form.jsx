import React from "react";
import "./Form.css";
import {useState} from "react";
import Input from "../Input";


function Form(props) {
  let {handleSubmit, handleInputName, handleInputRating, handleInputReview} = props;

  let [name, setName] = useState(localStorage.getItem("name"));
  let [rating, setRating] = useState(+localStorage.getItem("rating"));
  let [review, setReview] = useState(localStorage.getItem("review"));
  let [errorName, setErrorName] = useState("");
  let [errorRating, setErrorRating] = useState("");
   

  // Срабатывает при вводе имени
  handleInputName= (e)=>{
    setName(e.target.value);
    localStorage.setItem("name", e.target.value);
  };

  // Срабатывает при вводе рейтинга
  handleInputRating= (e)=>{
    setRating(e.target.value);
    localStorage.setItem("rating", e.target.value);
  };

  // Срабатывает при вводе текста отзыва
  handleInputReview= (e)=>{
    setReview(e.target.value);
    localStorage.setItem("review", e.target.value);
  };
  
  handleSubmit = (e) => {
    e.preventDefault();

    if (!name) {
      setErrorName("Вы забыли указать имя и фамилию");
      return;
    } else if (name.replaceAll(" ", "").length < 2) {
      setErrorName("Имя не может быть короче 2-х символов");
      return;
    } else {
      setErrorName("");
    }

    if (isNaN(rating) || rating < 1 || rating > 5 || !rating) {
      setErrorRating("Оценка должна быть от 1 до 5");
      return;
    } else {
      setErrorRating("");
    }
  
    localStorage.removeItem("name");
    setName('');
    localStorage.removeItem("rating");
    setRating('');
    localStorage.removeItem("review");
    setReview('');
    
    console.log("Ваш отзыв был успешно отправлен и будет отображён после модерации");
            
  }
 
  return (
    <div className="content-search content-search_size_m">
      <div className="content-search__title">Добавить свой отзыв</div>
      <form onSubmit={handleSubmit} className="search-form">
        <div className="search-form__item">
          <div className="input-box search-form__name"> 
            <Input
              focus={(e) => setErrorName("")}
              value={name || ""}
              input={handleInputName}
              type="text"
              className={`input ${errorName ? "border-red": ""}`}
              id="name"
              name="name"
              placeholder="Имя и фамилия"
            />
            {errorName && <div className="input-error">{errorName}</div>}
          </div>
          <div className="input-box search-form__rating">
            <Input
              input={handleInputRating}
              focus={(e) => setErrorRating("")}
              value={rating || ""}
              type="number"
              className={`input ${errorRating ? "border-red": ""}`}
              id="rating"
              name="rating"
              placeholder="Оценка"
            />
            {errorRating && <div className="input-error">{errorRating}</div>}
          </div>
        </div>
        <div className="search-form__item">
          <textarea
            onInput={handleInputReview}
            value={review || ""}
            className="textarea input"
            id="review"
            name="review"
            placeholder="Текст отзыва"
          />
        </div>
        <button className="btn search-form__btn" type="submit">
          Отправить отзыв
        </button>
      </form>
    </div>
  );
}

export default Form;
