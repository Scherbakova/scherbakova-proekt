import React from "react";
import "./Footer.css";
import LinkPage from "../LinkPage";
import DateYear from "../DateYear";


function Footer(props) {
  return (
    <footer className="footer page__footer">
      <div className="footer__content">
        <div className="footer__copyright">
          <div className="footer__logo-text">
            © ООО «<span className="footer__text-color">Мой</span>Маркет»,
            2018-<DateYear />.
          </div>
          <p className="footer__text">
            Для уточнения информации звоните по номеру 
            <LinkPage
              className={"footer__link"}
              href={"tel:79000000000"}
              text={"+7 900 000 0000 "}
            />,<br /> а предложения по сотрудничеству отправляйте на почту
            <LinkPage
              className={"footer__link"}
              href={"mailto:partner@mymarket.com"}
              text={"partner@mymarket.com"}
            />
          </p>
        </div>
        <div className="footer__up">
            <LinkPage
                className={"footer__link"}
                href={"#"}
                text={<span className="footer__text-up">Наверх</span>}
            />
        </div>
      </div>
    </footer>
  );
}

export default Footer;
