import React from "react";
import "./StarsRating.css";

import Star from "../Star";

function StarRating(props) {
    let {receivedRating=0}=props;
    let numberStars = 5;
    
    function getColor(number, receivedRating) {  
        return (number < receivedRating) ? "yellow": "grey";
    }

    return (
        <div className="feedback__rating">
           {Array.from({length: numberStars}).map((e, number) => (
                <Star
                    key={number}
                    color={getColor(number, receivedRating)}
                />
            ))}
            
        </div>
    );
}
export default StarRating;
