import React from "react";
import "./CounterIcon.css";

function CounterIcon(props) {
  let {className1, className2, className3, src, alt, id, count} = props;
  return (
    <div className="header__icon-counter">
      <div className="header__item-icon">
        <img className={className1} src={src} alt={alt} />
      </div>
      <div className={`header__item-counter ${className2}`}>
        <span className={`header__number ${className3}`} id={id}>{count}</span>
      </div>
    </div>
  );
}

export default CounterIcon;
