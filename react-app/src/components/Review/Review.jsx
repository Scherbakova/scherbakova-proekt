import React from "react";
import "./Review.css";
import StarRating from "../StarsRating";

function Review(props) {
  let {review, selected}=props;
  let className = `feedback__content ${selected ? "feedback__content_separator": ""}`;
  
  return (
    <div className="feedback">
      <img
        className="feedback__foto"
        src= {review.photoАuthor.src}
        title="Фотография покупателя"
        alt= {review.photoАuthor.alt}
      />
      <div className={className}>
        <div className="feedback__header">
          <p className="feedback__name">
            <b>{review.name}</b>
          </p>
          <StarRating receivedRating= {review.rating} />
        </div>
        <div className="feedback__info">
          <p className="feedback__text">
            <b>Опыт использования:</b> {review.testTime}
          </p>
          <p className="feedback__text">
            <b>Достоинства:</b> {'\n'}
            {review.plusProduct}
          </p>
          <p className="feedback__text">
            <b>Недостатки:</b> {'\n'}
            {review.minusProduct}
          </p>
        </div>
      </div>
    </div>
  );
}

export default Review;
