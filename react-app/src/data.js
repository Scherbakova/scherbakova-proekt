let product = {
  title: "Смартфон Apple iPhone 13",
  id: 368,
  priceNewRub: "67 990₽",
  priceOldRub: "75 990₽",
  discount: "-8%",
  imgProductLinks: [
    "./images/img/image-1.webp",
    "./images/img/image-2.webp",
    "./images/img/image-3.webp",
    "./images/img/image-4.webp",
    "./images/img/image-5.webp",
  ],
  selectedColor: "Синий",
  colors: [
    {src: "/colorRed.webp", title: "Красный", alt: "Красный смартфона AppleiPhone 13"},
    {src: "/colorGreen.webp", title: "Зеленый", alt: "Зеленый смартфона AppleiPhone 13"},
    {src: "/colorPink.webp", title: "Розовый", alt: "Розовый смартфона AppleiPhone 13"},
    {src: "/colorBlue.webp", title: "Синий", alt: "Синий смартфона AppleiPhone 13"},
    {src: "/colorWhite.webp", title: "Белый", alt: "Белый смартфона AppleiPhone 13"},
    {src: "/colorBlack.webp", title: "Черный", alt: "Черный смартфона AppleiPhone 13"},
  ],
  selectedMemoryGb: 128,
  memoryGb: [128, 256, 512],
  productCharacteristics: {
    screen: 6.1,
    memoryGb: 128,
    operatingSystem: "iOS 15",
    wirelessInterfaces: ["NFC", "Bluetooth", "Wi-Fi"],
    processor: "Apple A15 Bionic",
    weightGrams: 173,
  },
  description: `Наша самая совершенная система двух камер.
  Особый взгляд на прочность дисплея.
  Чип, с которым всё супербыстро.
  Аккумулятор держится заметно дольше.
  iPhone 13 - сильный мира всего.
  
  Мы разработали совершенно новую схему расположения и развернули объективы на 45 градусов. 
  Благодаря этому внутри корпуса поместилась наша лучшая система двух камер с увеличенной матрицей широкоугольной камеры. 
  Кроме того, мы освободили место для системы оптической стабилизации изображения сдвигом матрицы. 
  И повысили скорость работы матрицы на сверхширокоугольной камере.
  
  Новая сверхширокоугольная камера видит больше деталей в тёмных областях снимков. 
  Новая широкоугольная камера улавливает на 47% больше света для более качественных фотографий и видео. 
  Новая оптическая стабилизация со сдвигом матрицы обеспечит чёткие кадры даже в неустойчивом положении.
  
  Режим «Киноэффект» автоматически добавляет великолепные эффекты перемещения фокуса и изменения резкости. 
  Просто начните запись видео. Режим «Киноэффект» будет удерживать фокус на объекте съёмки, создавая красивый эффект размытия 
  вокруг него.   Режим «Киноэффект» распознаёт, когда нужно перевести фокус на другого человека или объект, который появился в кадре.
  Теперь ваши видео будут смотреться как настоящее кино.`,
  delivery: [
    {name: "Самовывоз", date: "четверг, 1 сентября", cost: 0},
    {name: "Курьером", date: "четверг, 1 сентября", cost: 0},
  ],
  reviews: [
    {
      id:1,
      photoАuthor: {src: "/photoАuthor1.jpg", alt: "Мужчина в очках"},
      name: "Марк Г.",
      rating: "5",
      testTime: "менее месяца",
      plusProduct: `это мой первый айфон после после огромного количества телефонов на андроиде. всё плавно, чётко и красиво. довольно шустрое устройство. камера весьма неплохая, ширик тоже на высоте.`,
      minusProduct: `к самому устройству мало имеет отношение, но перенос данных с андроида - адская вещь) а если нужно переносить фото с компа, то это только через itunes, который урезает качество фотографий исходное`,
    },
    {
      id:2,
      photoАuthor: {src: "/photoАuthor2.jpg", alt: "Мужчина 40лет"},
      name: "Валерий Коваленко",
      rating: "4",
      testTime: "менее месяца",
      plusProduct: `OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго`,
      minusProduct: `Плохая ремонтопригодность`,
    }
  ]
};

export default product;
