import React from "react";
import "./PageProduct.css";
import Header from "../components/Header";
import Breadcrumbs from "../components/Breadcrumbs";
import ImagesList from "../components/ImagesList";
import SliderImges from "../components/SliderImges";
import Colors from "../components/Colors";
import Configs from "../components/Configs";
import Characteristics from "../components/Characteristics";
import Description from "../components/Description";
import Comparison from "../components/Comparison";
import SidebarBasket from "../components/SidebarBasket";
import Advertising from "../components/Advertising";
import Reviews from "../components/Reviews";
import Form from "../components/Form";
import Footer from "../components/Footer";

import product from "../data";


function PageProduct() {
  return (
    <div className="page">
      <div className="page__content">
        <Header />
        <div className="page__inner">
          <Breadcrumbs />
          <main className="main-content">
            <div className="main-content__title">
              <h2 className="main-content__title-text">
              {product.title}, {product.selectedColor}
              </h2>
            </div>
            <ImagesList/>
            <SliderImges/>
            <div className="box-characteristic main-content__box-characteristic">
              <div className="all-characteristic all-characteristic_size_m">
                <Colors />
                <Configs />
                <Characteristics />
                <Description />
                <Comparison />
              </div>
              <div className="sidebar sidebar_size_s">
                <SidebarBasket product={product}/>
                <Advertising/>
              </div>
            </div>
            <Reviews reviews={product.reviews}/>
            <Form/>
          </main>
        </div>
      </div>
      <Footer/>
    </div>
  );
}

export default PageProduct;
