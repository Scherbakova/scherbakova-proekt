import React from "react";
import "./PageError.css";
import Header from "../components/Header";
import Footer from "../components/Footer";
import {Link} from "react-router-dom";

function PageError(props) {
  return (
    <div className="PageError">
      <div className="PageError__content">
        <Header />
          <main className="PageError__main">
            <div className="PageError__text">
              <div className="PageError__error">404</div>
              <p>Такой страницы нет!</p>
              <p>Можно перейти на <Link to="/" className={"PageError__link"}>главную страницу сайта</Link></p>
            </div>
          </main>
      </div>
      <Footer/>
    </div>
  );
}

export default PageError;