import React from "react";
import {BrowserRouter, Routes, Route} from "react-router-dom";
import PageProduct from "./PageProduct";
import PageIndex from "./PageIndex";
import PageError from "./PageError";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<PageIndex />} />
        <Route path="/product" element={<PageProduct />} />
        <Route path="*" element={<PageError />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;