import React from "react";
import "./PageIndex.css";
import Header from "../components/Header";
import Footer from "../components/Footer";

import {Link} from "react-router-dom";

function PageIndex(props) {
  return (
    <div className="PageIndex">
      <div className="PageIndex__content">
        <Header />
          <main className="PageIndex__main">
            <div className="PageIndex__text">
              Здесь должно быть содержимое главной страницы.{'\n'} Но в рамках курса главная страница  используется лишь{'\n'} в демонстрационных целях<br/>
            </div>
            <Link to="/product" className={"PageIndex__link"}>Перейти на страницу товара</Link>
          </main>
      </div>
      <Footer/>
    </div>
  );
}

export default PageIndex;
