import { createSlice } from "@reduxjs/toolkit";

export let heartSlice = createSlice({
  name: "heart",
  // Начальное состояние избранного
  initialState: { products: [] },

  reducers: {
    // Добавляем товар в избранное
    addSelected: (prevState, action) => {
      // Внутри action.payload информация о добавленном товаре
      let product = action.payload;
      
      let newState = {
        ...prevState,
        // Возвращаем новый массив товаров вместе с добавленным
        products: [...prevState.products, product],
      };
      return newState;
    },

    // Удаляем товар из избранное
    removeSelected: (prevState, action) => {
      // Внутри action.payload информация о добавленном товаре
      let product = action.payload;
      
      return {
        ...prevState,
        // Возвращаем новый массив товаров без удаленного
        products: prevState.products.filter((prevProduct) => {
          return prevProduct.id !== product.id;
        }),
      };
    },
  },
});
// Экспортируем наружу все действия
export let { addSelected, removeSelected } = heartSlice.actions;
// И сам редуктор тоже
export default heartSlice.reducer;
