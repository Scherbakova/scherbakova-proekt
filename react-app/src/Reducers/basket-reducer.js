import {createSlice} from "@reduxjs/toolkit";


export let basketSlice = createSlice({
  name: "basket",
  //Начальное состояние корзины
 initialState: { products: [] },
  
  reducers: {
    // Добавляем товар в корзину
    addProduct: (prevState, action) => {
      // Внутри action.payload информация о добавленном товаре
      let product = action.payload;
      //console.log("action", action)
      let newState = {
        ...prevState,
        // Возвращаем новый массив товаров вместе с добавленным
        products: [...prevState.products, product],
      };
      return newState; 
    },

    // Удаляем товар из корзины
    removeProduct: (prevState, action) => {
      let product = action.payload;
      
      let newState = {
        ...prevState,
        // Возвращаем новый массив товаров без удаленного
        products: prevState.products.filter((prevProduct)=> {
        return prevProduct.id !== product.id;
        })
      };
      
      return newState;
    }
  }
});
// Экспортируем наружу все действия
export let {addProduct, removeProduct} = basketSlice.actions;
// И сам редуктор тоже
export default basketSlice.reducer;
