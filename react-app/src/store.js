import { configureStore } from "@reduxjs/toolkit";
import basketReducer from "./Reducers/basket-reducer";
import heartReducer from "./Reducers/heart-reducer";

//Это middleware logger
let logger = (store) => (next) => (action) => {
  console.log("action", action);

  // Функция next применяет действие к хранилищу
  // и возвращает новое состояние хранилища
  let result = next(action);

  // Выводим в консоль новое состояние
  console.log("next state", store.getState());
  return result;
};

// Задаем начальное состояние счетчика
let counter = [];

let counterAction = (store) => (next) => (action) => {
  // Добавляем в счетчик действия
  counter.push(action);

  // Выводим количество действий в консоль
  console.log(`Количество обработанных действий:${counter.length}`);
  return next(action);
};

export let store = configureStore({
  reducer: {
    basket: basketReducer,
    heart: heartReducer,
  },

  // Подключаем middleware ко всем хранилищу
  // Он будет работать для ВСЕХ действий сразу!
  middleware: [logger, counterAction],
});
